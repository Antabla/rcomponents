import { TemplateRef } from '@angular/core';

export interface Column {
  title: string;
  data: string;
  class?: string;
  sortable?: boolean;
  filterable?: boolean;
  template?: TemplateRef<any>;
  render?: (data:any) => string;
  showIf?: (data: any) => boolean;
  rowClass?: (data:any) => string;
}

/**
 *
 * title: the title used in the column header
 * data: the name of the column data inside the json
 * sortable: defines whether a column is sortable or not
 * filterable: defines whether a column is filterable or not
 * template: angular template for rendering dynamic components
 * render: function that renders non-dynamic elements
 * showIf:function that validates if a field should be shown or not
 *
 */
