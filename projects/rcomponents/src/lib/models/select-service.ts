import { Observable } from 'rxjs';

export interface SelectService {
  callback: (value: any, search: string) => Observable<any>;
}

/**
 *
 * value:   Envia el valor actual del select por si se requiere para hacer algun filtro en el back
 * search:  Valor por el cual se quiere filtrar
 *
 *
 * El resultado esperado por la funcion deberia ser un array de objetos que deberia estar
 * parseado al tipo Option
 *
 */
