import { Observable } from "rxjs";

export interface TableService {
  callback: (params:any, filters:any) => Observable<any>
}

/**
 *
 * params: parametros para la paginacion y filtrado del lado del servidor
 *     Ej:
 *        {
 *            start: pagina actual
 *            length: numero de elementos que traera
 *            order: {
 *              index: indice de la columna por la cual ordenas
 *              key: columna por la cual ordenar
 *              dir: modo de ordenamiento asc o desc
 *            },
 *            search: {
 *              key: columna por la cual buscar
 *              value: valor de la busqueda
 *            },
 *        }
 *
 * filters: campos externos a la tabla para aplicar filtros
 *     Ej:
 *        {
 *            campo1: 'valor1',
 *            campo2: 'valor2',
 *            ....
 *        }
 *
 *
 * El resultado esperado por la funcion deberia ser
 *    {
 *      data: array de datos
 *      recordsTotales: numero de registros
 *    }
 *
 */
