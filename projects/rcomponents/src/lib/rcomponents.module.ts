import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RInputComponent } from './components/r-input/r-input.component';
import { RSelectComponent } from './components/r-select/r-select.component';
import { RTableComponent } from './components/r-table/r-table.component';

const COMPONENTS = [
  RInputComponent,
  RSelectComponent,
  RTableComponent
];

const MODULES = [CommonModule,ReactiveFormsModule, FormsModule ];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  exports: [...COMPONENTS,...MODULES],
})
export class RcomponentsModule {}
