import {
  Component,
  ElementRef,
  forwardRef,
  HostListener,
  Input,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Option } from '../../models/option';
import { SelectService } from '../../models/select-service';

@Component({
  selector: 'r-select',
  templateUrl: './r-select.component.html',
  styleUrls: ['./r-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RSelectComponent),
      multi: true,
    },
  ],
})
export class RSelectComponent implements ControlValueAccessor, OnInit {
  @Input() label: string = '';
  @Input() class: string = '';
  @Input() size: string = '';
  @Input() multiple: boolean = false;
  @Input() clearable: boolean = true;
  @Input() options: Option[] | any[] = [];
  @Input() showLoader: boolean = true;
  @Input() selectService: SelectService;
  @Input() bindLabel: string = '';
  @Input() bindValue: string = '';
  @Input() templateOption: TemplateRef<any>;

  open: boolean = false;
  search: string = '';
  filteredOptions: Option[] = [];
  loading: boolean = false;

  // CONTROL VALUE ACCESSOR VARIABLES
  _value: any;
  onChange = (_: any) => {};
  onTouched = () => {};
  isDisabled: boolean = false;

  constructor(private eRef: ElementRef) {}

  async ngOnInit() {
    await this.parseOptions();
    await this.filterOptions();
  }

  // CONTROL VALUE ACCESSOR METODOS
  get value(): any {
    return this._value;
  }

  set value(val: any) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  writeValue(value: any) {
    this.value = value;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  // METODOS PROPIOS DEL COMPONENTES

  // Metodo que filtra el listado de opciones, segun la busqueda especificada
  async filterOptions() { 
    if (this.search) {
      let options = this.options.filter(
        (opt) =>
          opt.label.toLowerCase().indexOf(this.search.toLowerCase()) != -1
      );

      if (!options || options?.length == 0) {
        await this.parseOptions(this.search);
        this.filteredOptions = this.options;
      } else {
        this.filteredOptions = options;
      }
    } else {
      this.filteredOptions = this.options;
    }
  }

  // Metodo que parsea el listado de opciones al formato correspondiente de ser necesario
  async parseOptions(search: string = '') {
    if (this.options?.length == 0 || search) {
      this.loading = true;
      let _options = await this.selectService
        .callback(this.value, search)
        .toPromise();
      this.options = _options;
    }

    let firstOption = this.options[0];

    if (!firstOption?.label || !firstOption?.value) {
      let _options = this.options.map((opt) => ({
        label: opt[this.bindLabel],
        value: this.bindValue ? opt[this.bindValue] : opt,
      }));
      this.options = _options;
    }

    this.loading = false;
  }

  // Metodo que obtiene el label del valor seleccionado si se encuentra en la lista
  get selectedLabel(): string | any[] {
    if (!this.multiple) {
      let selectedOption;  
      if (this.value)
        selectedOption = this.options.find((opt) =>
          this.equalsObjects(opt.value, this.value)
        );

      return selectedOption ? selectedOption?.label : '';
    } else {
      let selectedOption: any[] = []; 
      if (this.value instanceof Array) {
        this.value.map((v: any) => {
          let _value = this.options.find((opt) =>
            this.equalsObjects(opt.value, v)
          );

          if (_value) {
            selectedOption.push(_value.label);
          }
        });
      }

      return selectedOption?.length > 0 ? selectedOption : [];
    }
  }

  // Metodo que despliega la lista de opciones del select
  showOptions(event: any) {
    let elementEvent = event.srcElement.classList[0];
    if (elementEvent != 'r-item-delete') {
      this.open = !this.open;
    }
  }

  // Metodo que oculta la lista de opciones del select cuando el componente pierde el foco
  @HostListener('document:click', ['$event'])
  hideOptions(event: any) {
    let elementEvent = event.srcElement.classList[0];
    if (
      !this.eRef.nativeElement.contains(event.target) &&
      elementEvent != 'r-item-delete'
    ) {
      this.open = false;
    }
  }

  // Metodo que define si un item de la lista esta seleccionado
  isItemSelected(value: any) {
    if (this.multiple) {
      if (!(this.value instanceof Array)) return false;
      return this.value.find((v: any) => this.equalsObjects(value, v));
    } else {
      return this.value == value;
    }
  }

  // Metodo que selecciona un item de la lista
  selectOption(selectedValue: any) {
    if (this.multiple) {
      if (!(this.value instanceof Array)) this.value = [];
      let index = this.value.findIndex((v: any) =>
        this.equalsObjects(v, selectedValue)
      );
      if (index != -1) {
        this.value.splice(index, 1);
        if (this.value.length == 0) this.value = null;
      } else {
        let _value = this.value;
        this._value.push(selectedValue);
        this.value = _value;
      }
    } else {
      this.search = '';
      this.value = selectedValue;
      this.open = false;
      this.filterOptions()
    }
  }

  // Metodo que limpia los items seleccionados
  clearSelect() {
    this.value = null;
  }

  get isLoading(): boolean {
    return this.showLoader && this.loading;
  }

  // Metodo que elimina un item cuando el select es de tipo multiple
  deleteItemSelected(label: any) {
    let item = this.options.find((opt: any) => opt.label === label);

    if (item) {
      let index = this.value.findIndex((v: any) =>
        this.equalsObjects(item.value, v)
      );
      let _value = this.value;
      this._value.splice(index, 1);
      if (this._value.length == 0) _value = null;
      this.value = _value;
    }
  }

  // Metodo que comprueba si dos objetos son iguales
  equalsObjects(obj1: any, obj2: any): boolean {
    let _obj1 = Object.entries(obj1);
    if (_obj1.length == 0) _obj1 = obj1;

    let _obj2 = Object.entries(obj2);
    if (_obj2.length == 0) _obj2 = obj2;

    return _obj1.toString() === _obj2.toString();
  }
}
