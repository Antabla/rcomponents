import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators'; 
import { Column } from '../../models/column';
import { TableService } from '../../models/table-service';

@Component({
  selector: 'r-table',
  templateUrl: './r-table.component.html',
  styleUrls: ['./r-table.component.scss'],
})
export class RTableComponent implements OnInit {
  @Input() columns: Column[] = [];
  @Input() data: any[] = [];
  @Input() filters: FormGroup;
  @Input() localData: boolean = false;
  @Input() serverSide: boolean = true;
  @Input() RELOAD: Subject<any>;
  @Input() tableService: TableService;

  @Output() EMIT = new EventEmitter();

  filter: string = ''; // Modelo del select de filtrado
  search: string = ''; // Modelo del input de busqueda
  size: number = 5; // Modelo del select del numero de registros por pagina
  sortBy: string = ''; // Modelo del nombre del campo por el que se esta ordenando
  ascSort: boolean = true; // Variable que define si el ordenamiento es ascendente o no
  pages: number[] = []; // Array de los numeros de las paginas que se mostraran
  actualPage: number = 1; // Pagina actual

  recordsTotal: number = 0; // Cantidad de registros, cuando el modo es serverSide

  constructor(
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.initTableAndSearch();
    this.listenFiltersEvents();
    this.filter =  this.columns[0]?.title;
  }

  initTableAndSearch(): void {
    if (!this.localData) {
      if (this.serverSide) {
        this.tableService.callback(this.parametersTable, this.filtersValues)
          .subscribe((res: any) => {
            this.data = res.data || res;
            this.recordsTotal = res.recordsTotal || 0;
            this.EMIT.emit({ accion: 'dataloaded', data: this.data });
            this.generatePagination();
          });
      } else {
        this.tableService.callback({}, this.filtersValues)
          .subscribe((res: any) => {
            this.data = res.data || res;
            this.EMIT.emit({ accion: 'dataloaded', data: this.data });
            this.generatePagination();
          });
      }
    } else {
      this.generatePagination();
    }
  }

  // Crea todos los parametros para la tabla, cuando esta en modo serverSida
  get parametersTable(): any {
    let index = this.columns.findIndex((c) => c.data == this.sortBy);
    index = index == -1 ? 0 : index;

    return {
      start: this.actualPage - 1,
      length: this.size,
      order: {
        index,
        key: this.sortBy,
        dir: this.ascSort ? 'asc' : 'desc',
      },
      search: {
        key: this.filter,
        value: this.search,
      },
    };
  }

  // Construye el JSON con los campos para filtrar
  get filtersValues(): any {
    var filtersJson: any = {};
    if (this.filters)
      Object.keys(this.filters.controls).forEach((nombre) => {
        var valorFiltro = this.filters.controls[nombre].value;
        if (valorFiltro != undefined && valorFiltro != null)
          filtersJson[nombre] = valorFiltro;
      });

    return filtersJson;
  }

  // Escucha los cambios en los filtros pasados
  listenFiltersEvents(): void {
    if (this.filters) {
      //Estamos la pendiente de cualquier cambio en los filtros y recargamos la tabla
      this.filters.valueChanges
        .pipe(debounceTime(500))
        .subscribe(() => this.initTableAndSearch());
    }

    if (this.RELOAD) this.RELOAD.subscribe(() => this.initTableAndSearch());
  }

  // Renderiza una celda ya sea con una funcion especial o simplemente pintando el valor
  renderData(data: any, column: Column) {
    const value: any = data[column.data];
    if (column.render) {
      return this.sanitizer.bypassSecurityTrustHtml(column.render(data));
    }
    return value;
  }

  // Devuelve una fila para una celda en base a una funcion
  renderRowClass(data: any) {
    let column = this.columns.find((c) => c.rowClass);
    if (!column) return '';
    return this.sanitizer.bypassSecurityTrustHtml(column.rowClass(data));
  }


  // Funcion que es llamada cada vez que se hace click sobre una columna que es ordenable
  // Actualiza las variables para ordenar por la columna, ya sea ascendete o descendentemente
  sortTableBy(column: Column): void {
    if (this.isSortable(column)) {
      this.ascSort = this.sortBy == column.data ? !this.ascSort : true;
      this.sortBy = column.data;
      if (this.serverSide) this.initTableAndSearch();
    }
  }

  // Funcion que cambia de pagina segun el numero de pagina pasado
  changePage(page?: number): void {
    if (page) this.actualPage = page;
    if (this.serverSide) {
      this.initTableAndSearch();
    } else {
      this.generatePagination();
    }
  }

  // Determina si una columna sirve como filtro o no
  isFilterable(column: Column): boolean {
    return column.filterable == undefined || column.filterable;
  }

  // Determina si una columna es ordenable
  isSortable(column: Column): boolean {
    return column.sortable == undefined || column.sortable;
  }

  // Calcula el numero de paginas, segun el tamaño de registros por pagina y la cantidad de registros
  get totalPages(): number {
    if (this.serverSide) {
      return Math.ceil(this.recordsTotal / this.size);
    }
    return Math.ceil(this.data.length / this.size);
  }

  // Genera los botones de paginacion a medida que se avanza
  generatePagination() {
    let total = this.totalPages;
 

    if (this.pages.length == 0) {
      let max = 1;
      for (let index = 1; index <= total; index++ && max++) {
        this.pages.push(index);
        if (max == 5) break;
      }
    } else {
      let left = this.actualPage - 2;
      let right = this.actualPage + 2;

      left = left <= 0 ? ((right = total >= 5 ? 5 : total), 1) : left;
      right = right > total ? ((left = total - 4), total) : right;

      this.pages = []; 

      for (let index = left; index <= right; index++) { 
        if(index >0)
          this.pages.push(index);
      }
    }
  }

  // funcion que filtra los datos para mostrarlos en la tabla
  get filterData(): any[] {
    if (this.serverSide) {
      return this.data;
    } else {
      return this.dataBySizeAndPage;
    }
  }

  // Trae solo el numero de registros necesarios segun el tamaño de registros por pagina y la pagina actual
  get dataBySizeAndPage(): any[] {
    return this.dataBySearch.slice(
      this.size * this.actualPage - this.size,
      this.size * this.actualPage
    );
  }

  // Filtra los registros por una columna y un criterio de busqueda
  get dataBySearch(): any[] {
    if (!this.filter || !this.search) return this.sortData;

    console.log(this.filter, this.search);

    return this.sortData.filter(
      (d) =>
        (d[this.filter] + '')
          .toLowerCase()
          .indexOf(this.search.toLowerCase()) != -1
    );
  }

  // ordenas los registros segun una columna y un orden ascedente o descendente
  get sortData(): any[] {
    if (!this.sortBy) return this.data;

    let data = this.data.sort((a: any, b: any) => {
      if (a[this.sortBy] < b[this.sortBy]) {
        return -1;
      }
      if (a[this.sortBy] > b[this.sortBy]) {
        return 1;
      }
      return 0;
    });

    return this.ascSort ? data.reverse() : data;
  }

}
