# RComponents

Esta libreria fue generada usando [Angular CLI](https://github.com/angular/angular-cli) versión 12.2.0, contiene 3 componentes: **RInput**, **RSelect**, **RTable**, todos son wrappers de componentes de Bootstrap que poseen funcionalidades extras para reducir código.
Estos componentes estan basados en la interfaz [ControlValueAccessor](https://angular.io/api/forms/ControlValueAccessor) de angular, para hacer uso de los ReactiveForms y los NgModel.

## Apariencia

Apariencia de los componentes

![](https://gitlab.com/Antabla/rcomponents/-/raw/main/src/assets/RComponents.jpeg)

## Instalacion

Use [NPM](https://pip.pypa.io/en/stable/) para instalar este componente.
 
Use este comando para versiones de angular 13 y versiones 7 de rxjs

```bash
npm install rcomponents@1.1.14
```
Use este comando para versiones de angular 12 y versiones 6 de rxjs

```bash
npm install rcomponents@1.1.13
```

## Uso

Bootstrap
-------------
Esta libreria usa clases de boostrap, asi que es necesario importar los respectivos archivos de bootstrap en el archivo '<angular.json>', la importacion del archivo js de boostrap es opcional

```javascript
"styles": [
              ...
              "node_modules/bootstrap/dist/css/bootstrap.min.css"
          ],
"scripts": ["node_modules/bootstrap/dist/js/bootstrap.min.js"]
```

Modulo
-------------
Importamos la librería en el modulo principal o donde se requiera.

```javascript
import { RcomponentsModule } from 'rcomponents';  // <---- IMPORT

@NgModule({
  declarations: [
    ...
  ],
  imports: [ 
    RcomponentsModule  // <---- IMPORT
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

component.html
-------------
Llamamos los componentes que necesitemos en nuestro HTML pasandole los parametros correspondientes

```html
<div class="container" [formGroup]="form">
    
  <h2>R-Input</h2>
  <r-input label="Text"  formControlName="text"></r-input>
  <r-input label="Number" type="number" formControlName="number"></r-input>
  <r-input label="Range" type="range" formControlName="range"></r-input>
  <r-input label="Textarea" type="textarea" formControlName="textarea"></r-input>
  <r-input label="File" type="file" formControlName="file"></r-input>
  <r-input label="Color" type="color" formControlName="color"></r-input>
  <r-input label="Time" type="time" formControlName="time"></r-input>
  <r-input label="Date" type="date" formControlName="date"></r-input>
  <r-input label="Checkbox" type="checkbox" formControlName="checkbox"></r-input>
  <r-input label="Radio" type="radio" group="radio" [options]="options" formControlName="radio"></r-input>
 
 <h2>R-Select</h2>
  <r-select label="Select" formControlName="select" [selectService]="loadPosts" bindLabel="title" [templateOption]="optionTemplate"></r-select>

  <ng-template #optionTemplate let-option="option">
    <h5>{{option.label}} <span class="badge bg-secondary">{{option.value.id}}</span></h5>
  </ng-template>
 
  <h2>R-Table</h2>
  <r-table [columns]="columns" [tableService]="loadUsers"></r-table> 
  
</div>
```

component.ts
-------------
Configuracion de los componentes

```javascript

// TemplateRef para renderizar las opciones del select de otra forma
@ViewChild('optionTemplate', { static: true }) optionTemplate: TemplateRef<any>;
  
// Formulario dinamico
    this.form = this.formBuilder.group({
      text: [{ value: 'something', disabled: true }],
      number: null,
      range: null,
      textarea: null,
      file: null,
      color: null,
      time: null,
      date: null,
      checkbox: [{ value: false, disabled: true }],
      radio: 1,
      select: { name: 2 },
    });

    this.form.valueChanges.subscribe((value) => console.log(value));

    // Opciones de los selects o  input radio
    this.options = [
      {
        label: 'label1',
        value: { name: 1 },
      },
      {
        label: 'label2',
        value: { name: 2 },
      },
    ];

    // Definicion simple de columnas para la tabla
    this.columns = [
      {
        title: 'ID',
        data: 'id',
      },
      {
        title: 'NAME',
        data: 'name',
      },
    ];
  }

  // Funcion que obtiene los datos que llenaran la tabla
  get loadUsers(): TableService {
    return {
      callback: (_params: any, _filters: any) => {
        return this.http.get('https://jsonplaceholder.typicode.com/users');
      },
    };
  }
  
  // Funcion que obtiene los datos que llenaran el select
  get loadPosts(): SelectService{
    return {
      callback: (_value:any,_search:string) => {
        return this.http.get('https://jsonplaceholder.typicode.com/posts');
      }
    }
  }
```

Componentes
=============

## R-Input
Este componente es un wrapper del form-control de bootstrap, el cual reduce el codigo brindando un uso rapido y efectivo.

Los parametros que recibe este componente son:


Parametro | Tipo|Descripcion
------------- | ------------- | -------------
label  | string|Texto del label que acompaña el input
placeholder  | string| Texto del placeholder del input
type  | string| Texto con el tipo de input que se quiere pintar
class  |string| Nombre de clase para extender los estilos 
group  | string|Usado para agrupar los inputs de tipo radio y para agrupar los labels e inputs de los checkbox
size  | string| Valores permitidos **lg** o **sm**
options  | Option[]|Array de opciones de tipo Option
min  | number|valor minimo, usado para los inputs de tipo range 
max  | number|valor maximo, usado para los inputs de tipo range 

## R-Select
Este componente es un wrapper del **form-select** de bootstrap, el cual reduce el codigo brindando un uso rapido y efectivo, ademas de ser altamente parametrizable, dando opciones de busqueda local o en el servidor, a travez de un servicio.

Los parametros que recibe este componente son:


Parametro | Tipo|Descripcion
------------- | ------------- | -------------
label  | string|Texto del label que acompaña el input
class  |string| Nombre de clase para extender los estilos 
group  | string|Usado para agrupar los inputs de tipo radio y para agrupar los labels e inputs de los checkbox
size  | string| Valores permitidos **lg** o **sm**
options  | Option[]|Array de opciones de tipo Option
multiple  | boolean| Define si el select sera multiple o no, por defecto su valor es **false** 
clearable  | boolean| Define si el select mostrara un boton para limpiar la seleccion, por defecto su valor es **false** 
showLoader  | boolean| Define si el select mostrara un spinner de carga cuando se haga una busqueda al backend, por defecto su valor es **true** 
selectService  | SelectService| Este campo contiene la funcion que es pasada desde el componente padre, para hacer la consulta de datos al backend.
bindLabel  | string| Es el valor que sera mostrado por el select, esto es usado cuando se le pasa un array de objetos al select.
bindValue | string| Es el valor que sera guardado en el formulario dinamico o en el modelo, esto es usado cuando se le pasa un array de objetos al select, si este campo no se pasa, se tomara todo el valor del objeto, util para opciones con templates.
templateOption | TemplateRef| Por defecto el select muestra las opciones de manera simple, pero si se desea renderizar vistas mas complejas dentro de el, se debe pasar un templateRef.

## R-Table
Este componente es un datatable sencillo y configurable, que permite filtrar, paginar, ordenar, mostrar datos simples y datos complejos en sus celdas a traves del render de Angular. 

Los parametros que recibe este componente son:


Parametro | Tipo|Descripcion
------------- | ------------- | -------------
columns  | Column[]|Array de columnas utilizadas para renderizar los headers de la tabla ademas de los datos.
data  |any[]| Array de objectos que representan los datos.
filters  | FormGroup|Grupo de campos externos utilizados para filtrar la tabla.
localData  | boolean|Este flag determina si los datos se pasaran a traves de la variable **data** cuando se setea en **true** o si se obtendran de un servicio que apunte a un backend cuando se setea en **false**.
serverSide  | boolean|Define si las opciones de filtrado, paginacion y ordenamiento, se realizaran del lado del servidor o del lado del cliente.
RELOAD  | Subject | Este campo es usado para recargar los datos de la tabla desde el componente padre donde es llamada la tabla.
tableService  | TableService| Este campo de tipo TableService, contiene la funcion que es pasada desde el componente padre, para hacer la consulta de datos al backend.
EMIT  | TableService| A diferencia de los parametros anteriores este campo es un **Output** y sirve para enviar datos del componente hijo al padre, en caso de que sear necesario, por defecto esta emite los datos de la tabla cuando estos son cargados.


Modelos de datos
=============

Option
-------------

Esta interfaz representa los datos que deben pasarsele al componente  **RSelect** o al **RInput** de tipo radio.

Nombre | Tipo|Descripcion
------------- | ------------- | -------------
label  | string| Representa el valor que sera mostrado
value  | any| Representa el valor que sera guardado en la variable asociada al  **RSelect** o al **RInput** de tipo radio, cuando se escoja

Column
-------------

Esta interfaz representa la manera en que se deben configurar las columnas de la tabla.

Nombre | Tipo|Descripcion
------------- | ------------- | -------------
title  | string| El titulo usado en el header de la columna.
data  | string| El nombre del dato de la columna dentro del JSON de datos.
class  | string `<opcional>`| Clase para extender o modificar los estilos de la tabla.
sortable  | boolean`<opcional>`| Define cuando una columna es ordenable o no.
filterable  | boolean`<opcional>`| Define cuando una columna es filtrable o no.
template  | TemplateRef`<opcional>`|Template de Angular para renderizar componentes dinamicos.
render  | (data:any) => string`<opcional>`| Funcion que renderiza componentes no dinamicos. 
showIf  | (data:any) => boolean`<opcional>`| Funcion que valida en que condiciones se debe mostrar un campo. 
rowClass  | (data:any) => string`<opcional>`| Clase de estilos que afecta las filas de la tabla.

SelectService
-------------

Esta interfaz representa la estructura de la funcion que debe ser pasada al **RSelect** cuando los datos seran cargados desde el backend.

Nombre | Tipo|Descripcion
------------- | ------------- | -------------
callback  | function|  Esta funcion recibe dos parametros **value** y **search** el primero de tipo **any** y el segundo de tipo **string** y devuelve un array de objetos**.

value
-------------
Envia el valor actual del select por si se requiere para hacer algun filtro en el backend.

search
-------------
Valor por el cual se quiere filtrar.


TableService
-------------

Esta interfaz representa la estructura de la funcion que debe ser pasada al **RTable** cuando los datos seran cargados desde el backend.

Nombre | Tipo|Descripcion
------------- | ------------- | -------------
callback  | function|  Esta funcion recibe dos parametros **params** y **filters** ambos de tipo **any** y devuelve un **Observable**.

params
-------------
parametros para la paginacion y filtrado del lado del servidor.

```javascript
  {
  	start: 0, 			   // pagina actual
	  length: 5,  		   // numero de elementos que traera
	  order: {
		  index: 0,		  // 'indice de la columna por la cual ordenas'
		  key: 'ID',	      // 'columna por la cual ordenar'
		  dir:'asc',	      // 'modo de ordenamiento asc o desc'
	  },
	  search: {
		  key: 'ID',	     // 'columna por la cual buscar'
		  value: '123'	// 'valor de la busqueda'
	  }
  }
```

filters
-------------
campos externos a la tabla para aplicar filtros

```javascript
  {
		campo1: 'valor1',
		campo2: 'valor2',
		....
  }
```

Valor esperado por el backend para llenar la tabla 
-------------

```javascript
{
	data: 'array de datos',
	recordsTotales: 'numero de registros'
}
```
