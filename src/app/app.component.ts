import { Component, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Option } from './shared/models/option';
import Column from './shared/models/column';
import { TableService } from './shared/models/table-service';
import { SelectService } from 'projects/rcomponents/src/lib/models/select-service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  @ViewChild('optionTemplate', { static: true })
  optionTemplate: TemplateRef<any>;

  form: FormGroup;

  options: Option[];

  columns: Column[];

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {
    this.form = this.formBuilder.group({
      text: [{ value: 'something', disabled: true }],
      number: null,
      range: null,
      textarea: null,
      file: null,
      color: null,
      time: null,
      date: null,
      checkbox: [{ value: false, disabled: true }],
      radio: 1,
      select: { name: 2 },
    });

    this.form.valueChanges.subscribe((value) => console.log(value));

    this.options = [
      {
        label: 'label1',
        value: { name: 1 },
      },
      {
        label: 'label2',
        value: { name: 2 },
      },
    ];

    this.columns = [
      {
        title: 'ID',
        data: 'id',
      },
      {
        title: 'NAME',
        data: 'name',
      },
    ];
  }

  get loadUsers(): TableService {
    return {
      callback: (_params: any, _filters: any) => {
        console.log(_params);
        return this.http.get('https://jsonplaceholder.typicode.com/todos').pipe(
          map((d: any) => ({
            data: d.slice(_params.start * _params.length, _params.length + 1),
            recordsTotal: d.length,
          }))
        );
      },
    };
  }

  get loadPosts(): SelectService {
    return {
      callback: (value: any, search: string) => {
        return this.http.get('https://jsonplaceholder.typicode.com/posts');
      },
    };
  }
}
