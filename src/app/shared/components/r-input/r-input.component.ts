import {
  Component,
  forwardRef,
  Input,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Option } from '../../models/option';

@Component({
  selector: 'r-input',
  templateUrl: './r-input.component.html',
  styleUrls: ['./r-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RInputComponent),
      multi: true,
    },
  ],
})
export class RInputComponent implements ControlValueAccessor {
  @Input() label: string = '';
  @Input() placeholder: string = '';
  @Input() type: string = 'text';
  @Input() class: string = '';
  @Input() group: string = ''; // usado para agrupar los inputs de tipo radio y para agrupar los labels e inputs de los checkbox
  @Input() size: string = '';
  @Input() options: Option[] = [];
  @Input() min: number = 0;
  @Input() max: number = 100;

  _value: any;
  onChange = (_: any) => {};
  onTouched = () => {};
  isDisabled:boolean = false;

  constructor( ) {}

  get value(): any {
    return this._value;
  }

  set value(val: any) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  writeValue(value: number) {
    this.value = value;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
