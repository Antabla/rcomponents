import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RInputComponent } from './components/r-input/r-input.component';
import { RSelectComponent } from './components/r-select/r-select.component';
import { RTableComponent } from './components/r-table/r-table.component';
import { RGeneratorComponent } from './components/r-generator/r-generator.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 

const COMPONENTS = [
  RInputComponent,
  RSelectComponent,
  RTableComponent,
  RGeneratorComponent,
];

const MODULES = [ReactiveFormsModule, FormsModule ];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [CommonModule, ...MODULES],
  exports: [...COMPONENTS, ...MODULES],
})
export class SharedModule {}
